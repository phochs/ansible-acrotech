# Ansible Acrotech

This repository holds the Ansible setup for all servers within the Acrotech network.

## Setup

The control node for Acrotech's Ansible is a Raspberry pi. To setup the control node, perform the following actions:

1. Flash a clean Raspberry Pi image onto an SD card
1. Open the "Boot" volume and add a file named `ssh` (without extension)
1. Insert the SD card into a Raspberry Pi and boot it with network access
1. SSH into the Pi and execute this command (from the home folder):  
   `curl -s https://gitlab.com/phochs/ansible-acrotech/-/raw/main/ansible-install-controller | bash`
1. Go to the ansible-acrotech folder by executing `cd ansible-acrotech/`
1. Create a `.passwd` file and paste the Ansible vault password into it
1. Log in to each existing server with your own account and put the newly generated public key in `/home/ansible/.ssh/authorized_keys`

## Installing a new server (host)

The steps below describe how you add a new server to the system. This walks through the entire process, including installing Debian.

### Installing Debian

Load the latest Debian ISO onto the server and walk through the installation setup. Pay attention to these details:

- Use the default timeszones/locales
- Create a root user and the named user (Pijke)
- Bulk storage and storage for the services will usually be put in `/storage`. Keep this in mind when allocating disk space

### Setting up the server as an Ansible host

After Debian is installed and the server has booted into the fresh install for the first time, execute the tasks below from the project root in the controller.

1. Run `ansible-create-client` and follow the steps in the wizard. This installs the base requirements for Ansible to run
1. After the wizard is finished it gives an entry to add to the inventory. Usually servers go in the `primary` inventory file. Add the entry to the file and add any missing variables
1. Add the new server to the appropriate groups
1. Run the playbooks by executing this command: `ansible-playbook -l <server name> playbooks/_all.yml`. This will run all existing playbooks past the new server in the correct order
1. After this the server is ready for use! Check if you can log in and add any server specific services via new playbooks

## Executing playbooks

To execute a playbook, use the following command from the project root:  
`ansible-playbook playbooks/<playbook>.yml`

The following actions are performed automatically:

1. Load the global variables from `ansible.cfg`
1. Reading the Inventory files
1. Decrypt the group variables vault

## Installing additional requirements

When a role from the Ansible Galaxy is added to `requirements.yml`, it can be installed using the following command:  
`ansible-galaxy install -r requirements.yml`

Always add new roles via `requirements.yml`, this makes sure they are preserved when reinstalling the controller.

## Additional resources

Ansible user guide: https://docs.ansible.com/ansible/latest/user_guide/index.html  
Ansible collections: https://docs.ansible.com/ansible/latest/collections/index.html  
Ansible Galaxy docs: https://galaxy.ansible.com/docs/
