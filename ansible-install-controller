#!/bin/bash

# This script installs the software used to control the Acrotech setup on a Raspberry Pi.
# It might work on other platforms, but the script assumes an Ubuntu/Raspbian setup!
# This script is intended to only run once, but it is safe to run miltiple times,
# as long as it can see the .git folder or the ansible-acrotech folder.
#
# Run this script as a normal user, it will use sudo permissions when it needs to.
# Enjoy! See the echo'ed list below for an overview of the tasks this script will perform

echo "This script will install the an Ansible control node for the Acrotech network. The install will:"
echo " - Install Ansible"
echo " - Clone the repository into the ansible-acrotech/ folder"
echo " - Create a key pair for SSH connections to other machines"
echo " - Changes the user password"
echo " - Add your public SSH key to login"
echo " - Setup command line PATH"

sleep 3

## Install Ansible and Git
echo "...Installing Ansible and Git"

# Add the Ansible repository if it doesn't exist yet
if [[ ! -f "/etc/apt/sources.list.d/ansible.list" ]]; then
    . /etc/os-release
	
	# The repository is dependent on the Ubuntu release name, but this information isn't available on the OS, so it is recreated form the Debian release name
	ubuntu_codename=""
	case $VERSION_CODENAME in
		bullseye)
			ubuntu_codename="focal"
			;;
		buster)
			ubuntu_codename="bionic"
			;;
		stretch)
			ubuntu_codename="xenial"
			;;
		jessie)
			ubuntu_codename="trusty"
		;;
	esac
	
	# Create the Ansible source list
	echo "deb http://ppa.launchpad.net/ansible/ansible/ubuntu $ubuntu_codename main" | sudo tee -a /etc/apt/sources.list.d/ansible.list > /dev/null
fi

# Ansible can be installed know
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367 > /dev/null
sudo apt-get update > /dev/null
sudo apt-get install -y ansible git > /dev/null

## Install Git repository
echo "...Downloading the Git repository"

# Clone the Ansible Git repository into a new folder (ansible-acrotech) in the current pwd, if it doesn't exist yet
if [[ ! -d "./ansible-acrotech" && ! -d "./.git" ]]; then
    git clone git@gitlab.com:phochs/ansible-acrotech.git
fi

if [[ -d "./ansible-acrotech" ]]; then
	cd ansible-acrotech/
fi

## Generate a key pair
echo "...Generating SSH keys"

if [[ ! -f "$HOME/.ssh/ansible_id_rsa" ]]; then
    ssh-keygen -b 2048 -t rsa -f ~/.ssh/ansible_id_rsa -q -N ""
fi

## Change the user password
echo "Please provide a new password below:"
#passwd

## Add public SSH key to login
# This gives the user the option to add their public key to the controller now,
# so they will not get locked out when SSH is secured via the playbooks
echo "Paste your SSH public key here to add it to ~/.ssh/authorized_keys:"
read -p 'Public SSH key: ' ssh_key

if [ -z "$ssh_key" ]
then
	echo "No key provided, skipping"
else
	echo "Installing SSH key"
	mkdir ~/.ssh
	touch ~/.ssh/authorized_keys
	chmod 600 ~/.ssh/authorized_keys
	
	echo "$ssh_key" >> ~/.ssh/authorized_keys
fi

## Setup command line PATH
echo "...Setting up PATH"
current_pwd=$(pwd)

if ! grep -Fxq "export PATH=$current_pwd:\$PATH" ~/.bashrc
then
	echo "export PATH=$current_pwd:\$PATH" >> ~/.bashrc
	export PATH=$current_pwd:$PATH
fi

## Install Ansible Galaxy requirements
echo "... Installing Ansible Galaxy requirements"
ansible-galaxy install -r requirements.yml > /dev/null

echo ""

echo "We're done!"
echo "PLEASE, test your SSH key login before running Ansible playbooks, it will lock out password authentication"