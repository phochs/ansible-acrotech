#!/bin/bash

# Updates the motd message at each boot

echo "" > /etc/motd

echo "You have successfully logged in to $(cat /etc/hostname), $(cat /etc/issue)" >> /etc/motd

echo "" >> /etc/motd

paste /etc/ansible-files/motd_logo - <<< $(figlet -f /etc/ansible-files/big.flf -w 2000 $(cat /etc/hostname)) >> /etc/motd

echo "" >> /etc/motd

echo -e "\e[48;5;33m[NOTICE]\e[0m  This server is part of the Acrotech network." >> /etc/motd
echo -e "\e[48;5;208m\e[30m[WARNING]\e[0m This server is managed by Ansible. Production changes should be made there." >> /etc/motd
if grep -q production "/etc/ansible-files/server-type"; then
	echo -e "\e[48;5;208m\e[30m[WARNING]\e[0m This is a \e[41m\e[30mproduction\e[0m server." >> /etc/motd
fi

echo "" >> /etc/motd
echo "Enjoy your stay!" >> /etc/motd