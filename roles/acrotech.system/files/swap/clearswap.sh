#!/bin/bash

memfree=`cat /proc/meminfo | grep MemAvailable | awk '{print $2}'`
swap=`cat /proc/meminfo | grep SwapCached | awk '{print $2}'`

membuffer=$(expr $memfree / 2)

if [ $membuffer -gt $swap ]
then
  swapoff -a
  swapon -a
fi